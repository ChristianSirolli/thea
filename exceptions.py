import os, sys
import rfc3987
import configparser
class Exceptions:
	def __init__(self, e, exception, url, ids, reloadFunction):
		exc_type, exc_obj, exc_tb = exception
		fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
		self.exceptionobject = exc_obj
		self.exception = e
		self.errtype = exc_type
		self.filename = fname
		self.line = exc_tb.tb_lineno
		self.URL = url
		self.ids = ids
		self.reloadFunction = reloadFunction
		self.ids.__TheaSecureBtn__.background_color = [1, 1, 1, 1]
		self.ids.__TheaRefreshBtnImg__.source = 'emojis/black/1F504.png'
		errorTypes = {
			'Timeout': self.Timed_Out,
			# 'requests.exceptions.MissingSchema': self.Missing_Schema,
			'MissingSchema': self.Missing_Schema,
			# 'requests.exceptions.SSLError': self.SSL_Error,
			'SSLError': self.SSL_Error,
			# 'requests.exceptions.ConnectionError': self.Connection_Error,
			'ConnectionError': self.Connection_Error,
			# 'requests.exceptions.InvalidSchema': self.Invalid_Schema,
			'InvalidSchema': self.Invalid_Schema,
			'AttributeError': self.Attribute_Error,
			'TypeError': self.Type_Error
		}
		try:
			errorTypes[self.errtype.__name__]()
		except:
			print("\n/!\\ Exception:", self.exception)
	def Timed_Out(self): #Timeout
		print('\nTimed out in request to '+self.URL)
		print('Type:', self.errtype.__name__, 'at file', self.filename, 'at line', self.line, 'while browsing to', self.URL)
		# self.reloadFunction(text=self.URL)

	def Missing_Schema(self): #requests.exceptions.MissingSchema
		print('\nMissing Schema in URL:', self.URL, '\nRetrying with http://'+self.URL)
		print('Type:', self.errtype.__name__, 'at file', self.filename, 'at line', self.line, 'while browsing to', self.URL)
		newURL = 'http://'+self.URL
		try:
			rfc3987.parse(newURL, rule='URI')
		except ValueError:
			self.reloadFunction(self.config('general', 'search_engine_url').format(self.URL))
		else:
			self.reloadFunction(newURL)

	def SSL_Error(self): #requests.exceptions.SSLError
		print('\n/!\\ SSL Error:', self.exception, 'while browsing to', self.URL)
		print('Type:', self.errtype.__name__, 'at file', self.filename, 'at line', self.line, 'while browsing to', self.URL)
		self.ids.__TheaSecureBtn__.background_color = (1, 0, 0, 1)
		self.ids.__TheaRefreshBtnImg__.source = 'emojis/black/1F504.png'

	def Connection_Error(self): #requests.exceptions.ConnectionError
		print('\n/!\\ Connection Error:', self.exception, 'while browsing to', self.URL)
		print('Type:', self.errtype.__name__, 'at file', self.filename, 'at line', self.line, 'while browsing to', self.URL)

	def Invalid_Schema(self): #requests.exceptions.InvalidSchema
		print('\nInvalid Schema in URL:', self.URL, '\nRetrying with http://'+self.URL)
		print('Type:', self.errtype.__name__, 'at file', self.filename, 'at line', self.line, 'while browsing to', self.URL)
		newURL = 'http://'+self.URL
		try:
			rfc3987.parse(newURL, rule='URI')
		except ValueError:
			self.reloadFunction(self.config('general', 'search_engine_url').format(self.URL))
		else:
			self.reloadFunction(newURL)

	def Attribute_Error(self):
		print("\n/!\\ Attribute Error:", self.exception)
		print('Type:', self.errtype.__name__, 'at file', self.filename, 'at line', self.line, 'while browsing to', self.URL)

	def Type_Error(self):
		print("\n/!\\ Type Error:", self.exception)
		print('Type:', self.errtype.__name__, 'at file', self.filename, 'at line', self.line, 'while browsing to', self.URL)

	def config(self, group, setting):
		config = configparser.ConfigParser()
		config.read('config.ini')
		return config.get(group, setting)