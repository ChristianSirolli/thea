# from html.parser import HTMLParser
# import emoji
# import emoji_unicode
import json
from lxml import etree, html  # pip install lxml
from io import StringIO, BytesIO
import re
from exceptions import Exceptions
import sys
from thea import Browser

class ParseHTML:
	def __init__(self, data, ids, reloadFunction):
		try:
			self.data = data
			# print("\nContent:", self.data._content)
			print("\nContent Consumed:", self.data._content_consumed)
			print("\nNext:", self.data._next)
			print("\nStatus Code:", self.data.status_code)
			print("\nHeaders:", self.data.headers)
			print("\nContent-Type:", self.data.headers['Content-Type'])
			print("\nRaw:", self.data.raw.__dict__)
			print("\nURL:", self.data.url)
			print("\nEncoding:", self.data.encoding)
			print("\nHistory:", self.data.history)
			print("\nReason:", self.data.reason)
			print("\nCookies:", self.data.cookies)
			print("\nElapsed:", self.data.elapsed)
			print("\nRequest:", self.data.request.__dict__)
			print("\nConnection:", self.data.connection.__dict__)
			self.html = self.data.content
			# self.tree = html.document_fromstring(self.html)
			# print(html.tostring(self.tree))
			self.kv = """
ScrollableLabel:
	text: "{}"
			""".format(self.html.replace(b'"', b'\"'))
		except Exception as e:
			Exceptions(e, sys.exc_info(), self.data.url, ids, reloadFunction)

class ParseXML:
	def __init__(self, data, ids, reloadFunction):
		try:
			self.data = data
			print("Content:", self.data._content)
			print("\nContent Consumed:", self.data._content_consumed)
			print("\nNext:", self.data._next)
			print("\nStatus Code:", self.data.status_code)
			print("\nHeaders:", self.data.headers)
			print("\nContent-Type:", self.data.headers['Content-Type'])
			print("\nRaw:", self.data.raw)
			print("\nURL:", self.data.url)
			print("\nEncoding:", self.data.encoding)
			print("\nHistory:", self.data.history)
			print("\nReason:", self.data.reason)
			print("\nCookies:", self.data.cookies)
			print("\nElapsed:", self.data.elapsed)
			print("\nRequest:", self.data.request)
			print("\nConnection:", self.data.connection)
			self.xml = self.data.content
			self.tree = etree.fromstring(self.xml)
			print(etree.tostring(self.tree))
			self.kv = """
ScrollableLabel:
	text: "{}"
			""".format(self.xml.replace('"', '\"'))
		except Exception as e:
			Exceptions(e, sys.exc_info(), self.data.url, ids, reloadFunction)
	
class ParseJSON:
	def __init__(self, data, ids, reloadFunction):
		try:
			self.data = data
			print("Content:", self.data._content)
			print("\nContent Consumed:", self.data._content_consumed)
			print("\nNext:", self.data._next)
			print("\nStatus Code:", self.data.status_code)
			print("\nHeaders:", self.data.headers)
			print("\nContent-Type:", self.data.headers['Content-Type'])
			print("\nRaw:", self.data.raw)
			print("\nURL:", self.data.url)
			print("\nEncoding:", self.data.encoding)
			print("\nHistory:", self.data.history)
			print("\nReason:", self.data.reason)
			print("\nCookies:", self.data.cookies)
			print("\nElapsed:", self.data.elapsed)
			print("\nRequest:", self.data.request)
			print("\nConnection:", self.data.connection)
			self.json = json.dumps(json.loads(self.data.content), sort_keys=True, indent=4)
			self.kv = """
ScrollableLabel:
	text: "{}"
			""".format(self.json.replace("\n", "\\n").replace('"', '\\"'))
			print(self.kv)
		except Exception as e:
			Exceptions(e, sys.exc_info(), self.data.url, ids, reloadFunction)
		# https://support.oneskyapp.com/hc/en-us/article_attachments/202761627/example_1.json
# After Thea port to Kivy, Metatrepo needs to convert HTML/CSS/JavaScript to KV. Return a KV String.
# KV String is imported with `Builder.load_string(kv_string)`
