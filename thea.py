import metatrepo
from exceptions import Exceptions

import os
import sys
import re
import requests
from requests.exceptions import Timeout
import socket
import rfc3987
import configparser

import kivy
from kivy.base import EventLoop
from kivy.uix.boxlayout import BoxLayout
from kivy.app import App
from kivy.lang import Builder
from kivy.config import Config as kivy_config
from kivy.uix.label import Label
from kivy.uix.scrollview import ScrollView
from kivy.uix.widget import Widget
from kivy.uix.button import Button
from kivy.lang import Builder
from kivy.uix.screenmanager import ScreenManager, Screen
from kivy.uix.rst import RstDocument
from kivy.uix.tabbedpanel import TabbedPanel
# from kivy.properties import NumericProperty, StringProperty

kivy.require('1.10.1')
EventLoop.ensure_window()
kivy_config.set('kivy', 'window_icon', 'thea.png')
"""
# https://kivy.org/doc/stable/guide/environment.html
os.environ['KIVY_DATA_DIR'] = '<kivy path>/data'
os.environ['KIVY_MODULES_DIR'] = '<kivy path>/modules'
os.environ['KIVY_HOME'] = '/.kivy'
os.environ['KIVY_SDL2_PATH'] = ''
os.environ['KIVY_USE_DEFAULTCONFIG'] = ''
os.environ['KIVY_NO_CONFIG'] = '0'
os.environ['KIVY_NO_FILELOG'] = '0'
os.environ['KIVY_NO_CONSOLELOG'] = '1'
os.environ['KIVY_NO_ARGS'] = '1'
os.environ["KIVY_WINDOW"] = "sdl2 | pygame | x11 | egl_rpi"
os.environ['KIVY_TEXT'] = 'sdl2 | pil | pygame | sdlttf'
os.environ['KIVY_VIDEO'] = 'gstplayer | ffpyplayer | ffmpeg | null'
os.environ['KIVY_AUDIO'] = 'sdl2 | gstplayer | ffpyplayer | pygame | avplayer'
os.environ['KIVY_IMAGE'] = 'sdl2 | pil | pygame | imageio | tex | dds | gif'
os.environ['KIVY_CAMERA'] = 'avfoundation | android | opencv'
os.environ['KIVY_SPELLING'] = 'enchant | osxappkit'
os.environ['KIVY_CLIPBOARD'] = 'sdl2 | pygame | dummy | android'
os.environ['KIVY_DPI'] = ''
os.environ['KIVY_METRICS_DENSITY'] = ''
os.environ['KIVY_METRICS_FONTSCALE'] = ''
os.environ['KIVY_GL_BACKEND'] = ''
os.environ['KIVY_GL_DEBUG'] = ''
os.environ['KIVY_GRAPHICS'] = ''
os.environ['KIVY_GLES_LIMITS'] = '1'
os.environ['KIVY_BCM_DISPMANX_ID'] = '0: DISPMANX_ID_MAIN_LCD | 1: DISPMANX_ID_AUX_LCD | 2: DISPMANX_ID_HDMI | 3: DISPMANX_ID_SDTV | 4: DISPMANX_ID_FORCE_LCD | 5: DISPMANX_ID_FORCE_TV | 6: DISPMANX_ID_FORCE_OTHER'
os.environ['KIVY_BCM_DISPMANX_LAYER'] = '0'
"""
config = configparser.ConfigParser()
config.read('config.ini')
class ScrollableLabel(ScrollView):
	text = ''
class MorePanel(BoxLayout):
	pass
class TaskBarButton(Button):
	pass
class TabBar(TabbedPanel):
	pass
class DefaultTabScreen(Screen):
	pass
class NewTabScreen(Screen):
	document = RstDocument(source='theaTabs\\NewTab.rst')
class InsecureScreen(Screen):
	document = RstDocument(source='theaTabs\\Insecure.rst')
class NoConnectionScreen(Screen):
	document = RstDocument(source='theaTabs\\NoConnection.rst')
class TabCrashScreen(Screen):
	document = RstDocument(source='theaTabs\\TabCrash.rst')
class Browser(BoxLayout):
	def __init__(self, **kwargs):
		self.tabs = []
		try:
			super(Browser, self).__init__(**kwargs)
		except:
			pass
		self.orientation = "vertical"
		# self.padding = 1
		self.id = '__Thea__'
		self.headers = {
			'user-agent': 'Mozilla/5.0 (Windows NT 6.1; Win64; x64; rv:64.0) Gecko/20100101 Firefox/64.0 Thea/0.0.0.1'
		}
		# self.ids.__TheaScreenManager__.current = 'NewTabScreen'

	def config(self, group, setting):
		return config.get(group, setting)

	def newTab(self, title, data):
		self.tabs.append({
			'title': title,
			'data': data,
		})
	
	def is_url(self, url):
		url = url
		try:
			rfc3987.parse(url, rule='URI')
		except ValueError:
			url = 'http://'+url
			try:
				rfc3987.parse(url, rule='URI')
			except ValueError:
				return False
			else:
				return True
		else:
			return True
	
	def browseUrl(self, text):
		print('\n'+text)
		self.ids.__TheaSecureBtn__.background_color = [1, 1, 1, 1]
		self.ids.__TheaSecureBtn__.background_normal = 'atlas://data/images/defaulttheme/button'
		self.ids.__TheaSecureBtnImg__.source = 'emojis/black/1F513.png'
		self.ids.__TheaRefreshBtnImg__.source = 'emojis/black/F0001.png'
		self.URL = text
		if not self.is_url(self.URL):
			self.URL = self.config('general', 'search_engine_url').format(self.URL)
			self.ids.__TheaUrlBar__.text = self.URL
			return self.browseUrl(self.URL)
		# self.tabs[self.currentTab]["self.URL"] = self.URL
		try:
			self.req = requests.get(self.URL, headers=self.headers, timeout=(60, 300))
			if self.URL != self.req.url:
				self.ids.__TheaUrlBar__.text = self.req.url
				return self.browseUrl(self.req.url)
			self.parsePage(self.req, self.ids.__TheaRefreshBtnImg__)
			self.ids.__TheaSecureBtn__.background_color = (0, 1, 0, 1)
			self.ids.__TheaSecureBtnImg__.source = 'emojis/black/1F512.png'
			self.ids.__TheaRefreshBtnImg__.source = 'emojis/black/1F504.png'
		except Exception as e:
			Exceptions(e, sys.exc_info(), self.URL, self.ids, self.browseUrl)

	def parsePage(self, data, refresh):
		patterns = {
			'aac': re.compile('audio/aac.*', flags=re.I),
			'x-abiword': re.compile('application/x-abiword.*', flags=re.I),
			'x-freearc': re.compile('application/x-freearc.*', flags=re.I),
			'x-msvideo': re.compile('video/x-msvideo.*', flags=re.I),
			'vnd.amazon.ebook': re.compile('application/vnd.amazon.ebook.*', flags=re.I),
			'octet-stream': re.compile('application/octet-stream.*', flags=re.I),
			'bmp': re.compile('image/bmp.*', flags=re.I),
			'x-bzip': re.compile('application/x-bzip.*', flags=re.I),
			'x-bzip2': re.compile('application/x-bzip2.*', flags=re.I),
			'x-csh': re.compile('application/x-csh.*', flags=re.I),
			'css': re.compile('text/css.*', flags=re.I),
			'csv': re.compile('text/csv.*', flags=re.I),
			'msword': re.compile('application/msword.*', flags=re.I),
			'vnd.openxmlformats-officedocument.wordprocessingml.document': re.compile('application/vnd.openxmlformats-officedocument.wordprocessingml.document.*', flags=re.I),
			'vnd.ms-fontobject': re.compile('application/vnd.ms-fontobject.*', flags=re.I),
			'epub+zip': re.compile('application/epub+zip.*', flags=re.I),
			'gif': re.compile('image/gif.*', flags=re.I),
			'html': re.compile('text/html.*', flags=re.I),
			'vnd.microsoft.icon': re.compile('image/vnd.microsoft.icon.*', flags=re.I),
			'calendar': re.compile('text/calendar.*', flags=re.I),
			'java-archive': re.compile('application/java-archive.*', flags=re.I),
			'jpeg': re.compile('image/jpeg.*', flags=re.I),
			'javascript': re.compile('text/javascript.*', flags=re.I),
			'json': re.compile('application/json.*', flags=re.I),
			'ld+json': re.compile('application/ld+json.*', flags=re.I),
			'midi': re.compile('audio/midi audio/x-midi.*', flags=re.I),
			'audio/mpeg': re.compile('audio/mpeg.*', flags=re.I),
			'video/mpeg': re.compile('video/mpeg.*', flags=re.I),
			'vnd.apple.installer+xml': re.compile('application/vnd.apple.installer+xml.*', flags=re.I),
			'vnd.oasis.opendocument.presentation': re.compile('application/vnd.oasis.opendocument.presentation.*', flags=re.I),
			'vnd.oasis.opendocument.spreadsheet': re.compile('application/vnd.oasis.opendocument.spreadsheet.*', flags=re.I),
			'vnd.oasis.opendocument.text': re.compile('application/vnd.oasis.opendocument.text.*', flags=re.I),
			'ogg': re.compile('audio/ogg.*', flags=re.I),
			'ogg-alt': re.compile('video/ogg.*', flags=re.I),
			'ogg-alt2': re.compile('application/ogg.*', flags=re.I),
			'otf': re.compile('font/otf.*', flags=re.I),
			'png': re.compile('image/png.*', flags=re.I),
			'pdf': re.compile('application/pdf.*', flags=re.I),
			'vnd.ms-powerpoint': re.compile('application/vnd.ms-powerpoint.*', flags=re.I),
			'vnd.openxmlformats-officedocument.presentationml.presentation': re.compile('application/vnd.openxmlformats-officedocument.presentationml.presentation.*', flags=re.I),
			'x-rar-compressed': re.compile('application/x-rar-compressed.*', flags=re.I),
			'rtf': re.compile('application/rtf.*', flags=re.I),
			'x-sh': re.compile('application/x-sh.*', flags=re.I),
			'svg+xml': re.compile('image/svg+xml.*', flags=re.I),
			'x-shockwave-flash': re.compile('application/x-shockwave-flash.*', flags=re.I),
			'x-tar': re.compile('application/x-tar.*', flags=re.I),
			'tiff': re.compile('image/tiff.*', flags=re.I),
			'ttf': re.compile('font/ttf.*', flags=re.I),
			'plain': re.compile('text/plain.*', flags=re.I),
			'vnd.visio': re.compile('application/vnd.visio.*', flags=re.I),
			'wav': re.compile('audio/wav.*', flags=re.I),
			'audio/webm': re.compile('audio/webm.*', flags=re.I),
			'video/webm': re.compile('video/webm.*', flags=re.I),
			'webp': re.compile('image/webp.*', flags=re.I),
			'woff': re.compile('font/woff.*', flags=re.I),
			'woff2': re.compile('font/woff2.*', flags=re.I),
			'xhtml+xml': re.compile('application/xhtml+xml.*', flags=re.I),
			'vnd.ms-excel': re.compile('application/vnd.ms-excel.*', flags=re.I),
			'vnd.openxmlformats-officedocument.spreadsheetml.sheet': re.compile('application/vnd.openxmlformats-officedocument.spreadsheetml.sheet.*', flags=re.I),
			'xml': re.compile('application/xml.*', flags=re.I),
			'xml-alt': re.compile('text/xml.*', flags=re.I),
			'vnd.mozilla.xul+xml': re.compile('application/vnd.mozilla.xul+xml.*', flags=re.I),
			'zip': re.compile('application/zip.*', flags=re.I),
			'video/3gpp': re.compile('video/3gpp.*', flags=re.I),
			'audio/3gpp': re.compile('audio/3gpp.*', flags=re.I),
			'video/3gpp2': re.compile('video/3gpp2.*', flags=re.I),
			'audio/3gpp2': re.compile('audio/3gpp2.*', flags=re.I),
			'x-7z-compressed': re.compile('application/x-7z-compressed.*', flags=re.I),
		}
		tabContents = ''
		print(data.content)
		try:
			print('\n\n'+data.headers['Content-Type'])
			if patterns['html'].match(data.headers['Content-Type']):
				 tabContents = metatrepo.ParseHTML(data, self.ids, self.browserUrl).kv
			elif patterns['xml'].match(data.headers['Content-Type']) or patterns['xml-alt'].match(data.headers['Content-Type']):
				tabContents = metatrepo.ParseXML(data, self.ids, self.browserUrl).kv
			elif patterns['json'].match(data.headers['Content-Type']):
				tabContents = metatrepo.ParseJSON(data, self.ids, self.browserUrl).kv
			# Builder.load_string(tabContents)
			doc = ScrollView(size_hint=(1, None), size=(self.ids.__TheaWindow__.width, self.ids.__TheaWindow__.height))
			doc.add_widget(Builder.load_string(tabContents))
			self.ids.__TheaWindow__.add_widget(doc)
			print(tabContents)
			refresh.source = 'emojis/black/1F504.png'
		except Exception as e:
			Exceptions(e, sys.exc_info(), self.URL, self.ids, self.browseUrl)

class Thea(App):
	def build(self):
		self.icon = "thea.png"
		return Browser()


if __name__ == '__main__':
	Thea().run()
